function fibonacci(n) {
    let a = 0, b = 1;
    for (let i = 0; i < n; i++) {
        console.log(a);
        [a, b] = [b, a+b]
    }
}

console.time("Tempo de execução: ");
fibonacci(10);
console.timeEnd("Tempo de execução: ");
